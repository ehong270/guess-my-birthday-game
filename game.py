from random import randint

name = input("Hi! What is your name? ")

num_guesses = input("How many guesses does the computer get?\n")
num_guesses = int(num_guesses)

for guess_num in range(num_guesses):
    guessm = randint(1, 12)
    guessy = randint(1924, 2004)

    print("Guess:", guess_num+1, ":", name, "were you born on", guessm, "/", guessy, "?")
    answer = input("yes or no? ")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "no" and num_guesses-1 == guess_num:
        print("I have better things to do. Goodbye.")
    else:
        print("Drat! Lemme try again!")
